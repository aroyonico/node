const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const port = process.env.PORT || 3000;
const fs = require('fs');

app.use(express.static(__dirname + '/public'));

function shuffleArray(array) {
	for (let i = array.length - 1; i > 0; i--) {
			const j = Math.floor(Math.random() * (i + 1));
			const temp = array[i];
			array[i] = array[j];
			array[j] = temp;
	}
	return array;
}

function chooseRandomWords(nbWords) {
	const rawData = fs.readFileSync('words.json');
	const allWords = JSON.parse(rawData);
	let chosenWords = allWords.classic;
	chosenWords = shuffleArray(chosenWords);
	return chosenWords.splice(0, nbWords);
}

function randomizeTeams(teams) {
	const teamsInOrder = [];
	let teamToStart = null;
	for (let team in teams) {
		teams[team] = shuffleArray(teams[team]);
		if (teams[team].length == 3) {
			teamToStart = team;
		}
	}
	if (teamToStart != null) {
		teamsInOrder.push(teams[teamToStart]);
		teams.splice(teamToStart, 1);
	}
	while (teams.length > 0) {
		let index = Math.floor(Math.random() * teams.length);
		teamsInOrder.push(teams[index]);
		teams.splice(index, 1);
	}
	return teamsInOrder;
}

function onConnection(socket){
	socket.on('room-exists', (roomCode, callback) => {
		callback(io.sockets.adapter.rooms.get(roomCode) !== undefined);
	});

	socket.on('join-room', (roomCode, playerName, isOwner) => {
		const room = io.sockets.adapter.rooms.get(roomCode);
		const playersInRoom = [];
		if (room) {
			for (var socketId of room) {
				playersInRoom.push(io.sockets.sockets.get(socketId));
			}
		}
		socket.join(roomCode);
		socket.nickname = playerName;
		socket.isowner = isOwner;
		socket.to(roomCode).emit('joined-room', socket.id, playerName, isOwner);
		for (let player of playersInRoom) {
			io.to(socket.id).emit('joined-room', player.id, player.nickname, player.isowner);
		}
	});

	socket.on('disconnecting', () => {
		for (roomCode of socket.rooms) {
			socket.to(roomCode).emit('left-room', socket.id);
			if (socket.isowner) {
				const room = io.sockets.adapter.rooms.get(roomCode);
				if (room && room.size > 1) {
					const iter = room.values();
					let newOwner = iter.next().value;
					if (newOwner == socket.id) newOwner = iter.next().value;
					io.sockets.sockets.get(newOwner).isowner = true;
					socket.to(roomCode).emit('new-owner', newOwner);
				}
			}
		}
	});

	socket.on('choosing-teams', (roomCode) => {
		io.in(roomCode).emit('choosing-teams');
	});

	socket.on('join-team', (teamNumber, roomCode) => {
		io.in(roomCode).emit('joined-team', socket.id, teamNumber);
	});

	socket.on('start-game', (roomCode, teams) => {
		const room = io.sockets.adapter.rooms.get(roomCode);
		const words = chooseRandomWords(40);
		room.words = words;
		room.currentWords = words;
		const teamsInOrder = randomizeTeams(teams);
		room.teams = teamsInOrder;
		room.currentTeam = 0;
		room.currentPlayer = 0;
		const clueGiver = room.teams[room.currentTeam][room.currentPlayer];
		const guessers = room.teams[room.currentTeam].filter(function (v, i, a) {
			return v.sId != clueGiver.sId;
		});
		io.in(roomCode).emit('start-game', clueGiver, guessers, room.currentWords);
	});

	socket.on('launch-countdown', (roomCode) => {
		socket.to(roomCode).emit('launch-countdown');
	});

	socket.on('word-found', (roomCode, word) => {
		const room = io.sockets.adapter.rooms.get(roomCode);
		room.currentWords = room.currentWords.filter(function (v,i,a) {
			return v != word;
		});
		socket.to(roomCode).emit('word-found', word);
	});

	socket.on('end-turn', (roomCode) => {
		socket.to(roomCode).emit('end-turn');
		const room = io.sockets.adapter.rooms.get(roomCode);

		room.currentTeam++;
		if (room.currentTeam == room.teams.length) {
			room.currentTeam = 0;
			room.currentPlayer++;
		}
		const clueGiver = room.teams[room.currentTeam][room.currentPlayer % room.teams[room.currentTeam].length];
		const guessers = room.teams[room.currentTeam].filter(function (v, i, a) {
			return v.sId != clueGiver.sId;
		});
		let newEvent = '';

		if (room.currentWords.length == 0) {
			room.currentWords = shuffleArray(room.words);
			newEvent = 'new-round';
		} else {
			room.currentWords = shuffleArray(room.currentWords);
			newEvent = 'new-turn';
		}

		setTimeout(function () {
			io.in(roomCode).emit(newEvent, clueGiver, guessers, room.currentWords);
		}, 5000);
	});

	socket.on('back-to-lobby', (roomCode) => {
		io.in(roomCode).emit('back-to-lobby');
	});
}

io.on('connection', onConnection);

http.listen(port, () => console.log('listening on port ' + port));
