'use strict';

const socket = io();
const pages = ['lobby', 'waiting-room', 'choosing-team', 'game'];
let owner = false;
let roomCode = '';
let players = [];
let teams = [];
let currentClueGiver = '';
let guessing = false;
let interval = null;
let currentWords = [];
let currentRound = 1;

function playerFromSID(sId) {
	return players.filter(function(v, i, a) {
		return v.sId == sId;
	})[0];
}

function showPage(page) {
	for (const p of pages) {
		$('.'+p).hide();
	}
	$('.'+page).show();
}

function addErrorMessage(errorField, field, message) {
	if (field) {
		$('.' + field).addClass('is-invalid');
	}
	if ($('.'+errorField).html()) {
		$('.'+errorField).html($('.'+errorField).html() + '<br/>' + message);
	} else {
		$('.'+errorField).html(message);
	}
	$('.'+errorField).show();
}

function hideLobbyError() {
	$('.room-code').removeClass('is-invalid');
	$('.player-name').removeClass('is-invalid');
	$('.lobby-error').text('');
	$('.lobby-error').hide();
}

function hideWaitingRoomError() {
	$('.waiting-room-error').text('');
	$('.waiting-room-error').hide();
}

function hideChoosingTeamError() {
	$('.choosing-team-error').text('');
	$('.choosing-team-error').hide();
}

function addPLayerInWaitingRoom(sId, playerName, isYou, isOwner) {
	let classes = 'list-group-item player-name-'+sId+' ';
	if (isYou) classes += 'player-you ';
	if (isOwner) classes += 'player-owner ';
	const li = '<li class="'+classes+'">'+playerName+'</li>';
	$('.player-list').append(li);
	players.push({sId: sId, playerName: playerName, isYou: isYou, isOwner: isOwner});
}

function removePlayerFromWaitingRoom(sId) {
	$('.player-name-'+sId).remove();
	players = players.filter(function(v, i, a) {
		return v.sId != sId;
	});
}

function changeOwnerFromWaitingRoom(sId) {
	$('.player-name-'+sId).addClass('player-owner');
	playerFromSID(sId).isOwner = true;
}

function onFirstJoin() {
	showPage('lobby');
	$('.title').text('Welcome to the Lobby!');

	$('.create-room').click( function () {
		roomCode = $('.room-code').val();
		const playerName = $('.player-name').val();
		socket.emit('room-exists', roomCode, (exists) => {
			hideLobbyError();
			if (!playerName || playerName.trim() === '') {
				addErrorMessage('lobby-error', 'player-name', 'Please enter your name.');
			}
			if (!roomCode) {
				addErrorMessage('lobby-error', 'room-code', 'Please enter the name of the room.');
			} else if (exists) {
				addErrorMessage('lobby-error', 'room-code', 'This room already exists. Please join this existing room or create a new one.');
			}
			if (!$('.lobby-error').text()) {
				socket.emit('join-room', roomCode, playerName, true);
				owner = true;
				joinRoom(roomCode, playerName);
			}
		});
	});
	$('.join-room').click( function () {
		roomCode = $('.room-code').val();
		const playerName = $('.player-name').val();
		socket.emit('room-exists', roomCode, (exists) => {
			hideLobbyError();
			if (!playerName || playerName.trim() === '') {
				addErrorMessage('lobby-error', 'player-name', 'Please enter your name.');
			}
			if (!roomCode) {
				addErrorMessage('lobby-error', 'room-code', 'Please enter the name of the room.');
			} else if (!exists) {
				addErrorMessage('lobby-error', 'room-code', 'This room doesn\'t exist. Please create it or join an existing one.');
			}
			if (!$('.lobby-error').text()) {
				socket.emit('join-room', roomCode, playerName, false);
				owner = false;
				joinRoom(roomCode, playerName);
			}
		});
	});
}

socket.on('joined-room', (sId, playerName, isOwner) => {
	addPLayerInWaitingRoom(sId, playerName, false, isOwner);
});

socket.on('left-room', (sId) => {
	removePlayerFromWaitingRoom(sId);
});

socket.on('new-owner', (sId) => {
	$('.owner-tools').hide();
	changeOwnerFromWaitingRoom(sId);
	if (sId == socket.id) {
		owner = true;
		$('.owner-tools').show();
	}
});

function joinRoom(roomCode, playerName) {
	$('.owner-tools').hide();
	showPage('waiting-room');
	if (owner) {
		$('.owner-tools').show();
	}
	$('.title').text('Welcome to the room ' + roomCode);
	addPLayerInWaitingRoom(socket.id, playerName, true, owner);
	$('.start-team-choice').click(function () {
		hideWaitingRoomError();
		if (players.length <= 3) {
			addErrorMessage('waiting-room-error', undefined, 'There must be at least 4 players.');
		} else {
			socket.emit('choosing-teams', roomCode);
		}
	});
}

socket.on('choosing-teams', () => {
	$('.title').text('Please choose a team');
	const nbTeams = Math.floor(players.length / 2);
	for (let i = 1; i <= nbTeams; i++) {
		$('.teams-in-choosing .team-number').append('<div class="col team">Team '+i+' <button type="button" class="btn btn-primary join-team-'+i+'">Join team '+i+'</button></div>');
		$('.teams-in-choosing .player-1').append('<div class="col player player-1-team-'+i+'">&nbsp;</div>');
		$('.teams-in-choosing .player-2').append('<div class="col player player-2-team-'+i+'">&nbsp;</div>');
		$('.teams-in-choosing .player-3').append('<div class="col player player-3-team-'+i+'">&nbsp;</div>');
		$('.join-team-'+i).click(function () {
			socket.emit('join-team', i, roomCode);
		});
		teams.push([]);
	}
	showPage('choosing-team');
	$('.start-game').click(function () {
		if (checkTeam()) {
			socket.emit('start-game', roomCode, teams);
		}
	});
});

socket.on('joined-team', (sId, teamNumber) => {
	playerFromSID(sId).team = teamNumber;
	if(teams[teamNumber-1].length >= 3) {
		return;
	}
	for (let i = 0 ; i < teams.length ; i++) {
		teams[i] = teams[i].filter(function(v, i, a) {
			return v.sId != sId;
		});
	}
	teams[teamNumber-1].push({sId: sId});
	$('.player-name-'+sId).remove();
	assignTeams();
});

socket.on('start-game', (clueGiver, guessers, words) => {
	currentWords = words;
	for (let team in teams) {
		let teamNumber = parseInt(team)+1;
		$('.teams-in-game .team-number').append('<div class="col team">Team '+teamNumber+': Score = <span class="team-'+teamNumber+'-score">0</span>');
		$('.teams-in-game .player-1').append('<div class="col player '+(playerFromSID(teams[team][0].sId).isYou ? 'player-you' : '')+'">'+playerFromSID(teams[team][0].sId).playerName+'</div>');
		$('.teams-in-game .player-2').append('<div class="col player '+(playerFromSID(teams[team][1].sId).isYou ? 'player-you' : '')+'">'+playerFromSID(teams[team][1].sId).playerName+'</div>');
		$('.teams-in-game .player-3').append('<div class="col player '+(teams[team][2] && playerFromSID(teams[team][2].sId).isYou ? 'player-you' : '')+'">'+(teams[team][2] ? playerFromSID(teams[team][2].sId).playerName : '&nbsp;')+'</div>');
	}
	$('.cards-left').text(words.length);
	showPage('game');
	$('.main-game').show();

	$('.ready-check').click( function() {
		socket.emit('launch-countdown', roomCode);
		$('.card .ready-check').hide();
		$('.card .card-name').text(currentWords[0]);
		launchCountdown(true);
	});

	$('.word-found').click(function () {
		const scoreClass = '.team-'+playerFromSID(currentClueGiver).team+'-score';
		$(scoreClass).text(parseInt($(scoreClass).text()) + 1);
		$('.cards-left').text(parseInt($('.cards-left').text()) - 1);
		let word = currentWords.shift();
		socket.emit('word-found', roomCode, word);
		if (currentWords.length > 0) {
			$('.card .card-name').text(currentWords[0]);
		} else {
			socket.emit('end-turn', roomCode);
			endTurn();
		}
	});

	$('.end-turn').click(function () {
		socket.emit('end-turn', roomCode);
		endTurn();
	});

	$('.pass-card').click(function () {
		currentWords.shift();
		if (currentWords.length > 0) {
			$('.card .card-name').text(currentWords[0]);
		} else {
			socket.emit('end-turn', roomCode);
			endTurn();
		}
	});

	startTurn(clueGiver, guessers);
});

socket.on('new-turn', (clueGiver, guessers, words) => {
	currentWords = words;
	startTurn(clueGiver, guessers);
});

socket.on('new-round', (clueGiver, guessers, words) => {
	currentRound++;
	if (currentRound == 4) {
		endGame();
	} else {
		currentWords = words;
		$('.end-turn').hide();
		$('.pass-card').show();
		$('.cards-left').text(words.length);
		startTurn(clueGiver, guessers);
	}
});

function startTurn(clueGiver, guessers) {
	currentClueGiver = clueGiver.sId;
	const iAmClueGiver = clueGiver.sId == socket.id;
	const iAmGuesser = guessers.some(g => g.sId == socket.id);

	let title = 'Round '+currentRound+'!<br />';
	if (iAmClueGiver) {
		title += 'I am ';
	} else {
		title += playerFromSID(clueGiver.sId).playerName;
		title += ' is ';
	}
	title += 'giving clues to ';
	if (iAmGuesser) {
		title += 'me';
		if (guessers.length == 2) {
			title += ' and ';
			title += guessers[0].sId == socket.id ? playerFromSID(guessers[1].sId).playerName : playerFromSID(guessers[0].sId).playerName;
		}
	} else {
		title += playerFromSID(guessers[0].sId).playerName;
		if (guessers.length == 2) {
			title += ' and ';
			title += playerFromSID(guessers[1].sId).playerName;
		}
	}

	$('.title').html(title);

	if (iAmClueGiver) {
		$('.card .card-name').text('Please click below to start the countdown.')
		$('.card .ready-check').show();
	} else {
		$('.card .card-name').text('Waiting for '+playerFromSID(clueGiver.sId).playerName+' to start the countdown.')
		$('.card .ready-check').hide();
	}
}

function endTurn() {
	$('.cluegiver-tools').hide();
	$('.card .card-name').text('Turn ended.');
	clearInterval(interval);
}

socket.on('word-found', (word) => {
	const scoreClass = '.team-'+playerFromSID(currentClueGiver).team+'-score';
	$(scoreClass).text(parseInt($(scoreClass).text()) + 1);
	$('.cards-left').text(parseInt($('.cards-left').text()) - 1);
	$('.card .card-name').text('Last found word: ' + word);
});

socket.on('end-turn', () => {
	endTurn();
});

socket.on('launch-countdown', () => {
	$('.card .card-name').text(playerFromSID(currentClueGiver).playerName + ' is giving clues');
	launchCountdown(false);
});

function launchCountdown(starter) {
	let secondsLeft = 30;
	$('.countdown').text(secondsLeft+'s');
	guessing = true;
	if (currentClueGiver == socket.id) {
		$('.cluegiver-tools').show();
	}
	interval = setInterval(function() {
		secondsLeft--;
		if (secondsLeft < 0) {
			guessing = false;
			$('.cluegiver-tools').hide();
			clearInterval(interval);
			if (starter) {
				socket.emit('end-turn', roomCode);
				endTurn();
			}
		} else {
			$('.countdown').text(secondsLeft+'s');
		}
	}, 1000);
}

function assignTeams() {
	const nbTeams = Math.floor(players.length / 2);
	for (let i = 1; i <= nbTeams; i++) {
		for (let j = 1; j <= 3 ; j++) {
			$('.player-'+j+'-team-'+i).removeClass('player-you');
			$('.player-'+j+'-team-'+i).removeClass('player-owner');
			let player = teams[i-1][j-1] ? playerFromSID(teams[i-1][j-1].sId) : undefined;
			$('.player-'+j+'-team-'+i).html(teams[i-1].length >= j ? player.playerName : '&nbsp;');
			if (player && player.isOwner) {
				$('.player-'+j+'-team-'+i).addClass('player-owner');
			}
			if (player && player.isYou) {
				$('.player-'+j+'-team-'+i).addClass('player-you');
			}
		}
	}
}

function checkTeam() {
	hideChoosingTeamError();
	// Checking that every player is in a team.
	for (let player of players) {
		let isInATeam = false;
		for (let team of teams) {
			if (team.some(p => p.sId == player.sId)) {
				isInATeam = true;
			}
		}
		if (!isInATeam) {
			addErrorMessage('choosing-team-error', undefined, 'Player '+player.playerName+' should be in a team.');
			return false;
		}
	}
	// Checking that each team has at least 2 players.
	for (let i = 1 ; i <= teams.length ; i++) {
		if (teams[i-1].length < 2) {
			addErrorMessage('choosing-team-error', undefined, 'Team '+i+' should have at least two players.');
			return false;
		}
	}
	return true;
}

function endGame() {
	$('.title').text('The game ended!');
	$('.main-game').hide();
	if (owner) {
		$('.end-game').show();
	}
	$('.back-to-lobby').click(function() {
		$('.end-game').hide();
		socket.emit('back-to-lobby', roomCode);
	});
}

socket.on('back-to-lobby', () => {
	location.reload();
});

onFirstJoin();
